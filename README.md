# Webcounter

## Rumos Lab

Simple Python Webcounter with redis 

## Build
docker build -t david2felix/webcounter:1.0.1 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis david2felix/webcounter:1.0.1

## Gitlab register

```
gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "production" \
--registration-token GR1348941LcDjzgRaNkUbD2zznxWV

gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "test" \
--registration-token GR1348941LcDjzgRaNkUbD2zznxWV
```

## push feito em casa